\documentclass{wssci}
%\def\bibsection{\section*{References}} %Activate if "References" do not appear at the start of bibliography

% Replace "000" in the line below by your paper number
\newcommand\papernumber{114CO-0399}

% Replace "Laminar Flames" in the line below by your paper topic
\newcommand\papertopic{Coal and biomass combustion and gasification}

\usepackage{graphicx}				% Use pdf, png, jpg, or eps with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{subfigure}
\usepackage[justification=centering]{caption}
\usepackage{placeins}
\usepackage{wrapfig}

\newcommand{\up}[1]{\textsuperscript{#1}}

\title{Integrating Intra-Particle Processes in Large Scale Simulation of Biomass Thermochemical Conversion}
\author{Himanshu Goyal\up{*}, Perrine Pepiot \up{\textdagger}\\
\small{\up{*}Chemical and Biomolecular Engineering, Cornell University}\\
\small{\up{\textdagger}Sibley School of Mechanical and Aerospace Engineering, Cornell University}\\
\small{Corresponding Author Email: hg345$@$cornell.edu}\\
}
\date{}							% Activate to display a given date or no date



%New environment for adding keywords
\newenvironment{keyword}{%
 \bfseries \small{\keywordheading {\it \textbf{}}}
}

\begin{document}
\maketitle

% ======================================== %
%% ABSTRACT
% ======================================== %
\begin{abstract}
The present study focuses on thermochemical conversion technologies for converting biomass into biofuel using fluidized bed reactors (FBR). Efficient conversion of biomass is still a challenge, and predictive Computational Fluid Dynamics (CFD) simulation tools have the potential to accelerate the development and deployment of new technologies. Yet, such tools are challenging to develop because of the complex multi-physics, multi-scale nature of the conversion process. In particular, while particle scale phenomena can affect the overall dynamics of the system, most CFD studies in the literature neglect the intra-particle processes and use an homogeneous sphere model to describe the conversion of biomass in reactor simulations. In this work, we evaluate the validity of such an assumption by developing an intra-particle heat and mass transfer model that describes the devolatilization of a porous solid spherical biomass particle and comparing it to an homogeneous model for conditions pertinent to biomass gasification in FBR. Special emphasis is placed on the description of the chemistry, with the use of a recently published semi-detailed kinetic mechanism to describe the devolatilization chemistry process.
\end{abstract}
\begin{keyword}
Biomass gasification, CFD, fluidized bed reactors, Intra-particle heat and mass transfer.
\end{keyword}


% ======================================== %
%% INTRODUCTION
% ======================================== %
\section{Introduction}
Advanced biofuel technologies, based on sustainable, non-food sources of feedstocks, are required to meet aggressive volume goals for biofuel deployment. Several different technologies exist for converting cellulosic biomass to biofuels~\cite{farrell2006ethanol,huber2006synthesis,foust2008biorefinery}, the predominant differentiation being the primary catalysis system (heat, enzymes, or chemicals). We focus here on thermochemical conversion technologies, which rely on heat and/or physical catalysts to convert biomass to an intermediate gas or liquid, followed by an additional conversion step to transform that intermediate into a biofuel. Thermochemical conversion provides significant advantages, such as the ability to robustly handle a wide range of feedstocks, and to produce various types of transportation fuels.
While considerable progress has been achieved for both gasification and pyrolysis routes for biofuels production~\cite{demirbas2007progress}, some technical challenges still need to be addresses to enable mass industrialization of these processes, mostly in terms of quality control, efficiency, and operation scale-up~\cite{hamelinck2006outlook}. 

Currently, design and scale-up of thermochemical conversion reactors are mostly empirically-based, relying heavily on expensive and lengthy pilot-scale reactor studies. Yet, measurements in those reactors are unlikely to be detailed enough to provide the knowledge, especially at the particle scale, necessary to efficiently optimize biomass conversion~\cite{van2008numerical}.
%Most of the time experiments cannot provide the local and detailed quantitative information necessary to understand the physics at the particle scale~\cite{van2008numerical}. 
Computational Fluid Dynamics (CFD) is increasingly being recognized as an essential tool in the optimization and design process, by providing a much more flexible and affordable framework to investigate the controlling chemical and physical processes leading to liquid or gas phase products. However, the complexity of the multiphase (gas and dispersed particle phase), reacting, and inherently multi-scale flow, involving a wide range of length scales (sub-millimeter to meter) and time scales (from very fast chemical reactions to the reactor scale flow), severely hinders the development of reliable and accurate tools and models~\cite{deen2007review}. 

Due to limited computational resources, larger scale simulations require the introduction of modeling assumption, or closures, in the governing equations, since the small-scale dynamics cannot be resolved by the computational grid. In particular, most of the simulations of biomass pyrolysis and gasification published in the literature use homogeneous particle assumptions with highly parameterized, global kinetic models~\cite{xue2011cfd,bruchmuller2013tar} to decrease the cost of the chemical equations to a minimum. This potentially restricts drastically the value of simulation-derived conclusions and observations. Yet, a growing body of advanced experimental work on the characterization of intra-particle structure and composition evolution for single biomass particles is providing better definitions of the micro-scale governing equations. Coupling the improved description of the biomass particle-scale conversion process coupled with the significant advances made over the past decade in the development of predictive, highly scalable numerical tools for non-reactive dense granular flow simulation, a real opportunity exists to establish CFD approaches as a viable alternative to empirical process development. 

%Use of simulations for accurate prediction of these reactors require detailed modeling of the underlying phenomena. Moreover, significant theoretical, modeling, and numerical advances have been made in the development of predictive, highly scalable numerical tools for non-reactive dense granular flow simulation. 
%Yet, the integration of reactive processes in those dense particulate flow solvers is for the most part accomplished through empirical and intuitive procedures, forgoing the rigorous derivation of averaged or filtered governing equations and closure models suitable for large-scale simulations followed in the non-reactive context. In particular, the combination of highly parameterized global chemical conversion models and homogeneous particle assumption, reduces the reliability and accuracy of the predictions, allowing only uncertain qualitative interpretations of the results.

The long-term objective of this work is the development of a Lagrange-Euler numerical framework for the detailed simulation of biomass gasification in fluidized bed-type reactors, with an emphasis on chemistry and intra-particle processes, in order to assess the sensitivity of FBR simulations to intra-particle transport and chemical models. This paper focuses on the intra-particle aspect, and provides new insights on the importance of transport processes in simulations at the scale of the biomass particle. In the following, two different approaches to model particle conversion are presented in Sec.~\ref{methods}, the first one considering the detailed heat and mass transport inside the biomass, the other based on an homogeneous particle assumption. Comparison of particle temperature, mass, and product evolutions are then described in Sec.~\ref{results}, providing a first assessment on the validity of the homogeneous particle assumption in CFD studies. Future integration with the large-scale multiphase solver NGA to confirm this assessment is outline in conclusion.

%In the present study, we develop an intra-particle model, which couples the transpose processes with the thermo-chemical reactions of biomass devolatilization. The reactions are represented by a semi-detailed chemical kinetic model~\cite{corbetta2014pyrolysis}. This intra-particle model is then compared with a lumped model for the particle sizes and conditions pertinent to a FBR for biomass gasification. Main focus of this study is to check the validity of a lumped model in the context of biomass gasification using FBR. 


% ======================================== %
%% METHODOLOGY
% ======================================== %
\section{Methods}
\label{methods}
%%
In this section, a short description of the governing equations of intra-particle and homogeneous particle model is presented. The thermo-chemical reactions of biomass devolatilization are represented by a chemical kinetic mechanism developed by Corbetta et al.~\cite{corbetta2014pyrolysis}. This kinetic model contains 24 reactions describing the devolatilization of cellulose, hemicellulose and lignin, which involve $n_s = 19$ solid species, and $n_g = 20$ gas phase species.
%%
\subsection{Description of the intra-particle model}
%%
A spherical 1D intra-particle model is developed based on the work of Park et al.~\cite{park2010experimental}. The model represents the devolatilization of a biomass particle heated through convection and radiation. Thermal energy converts the solid biomass into gas phase species and residual char. Transport processes occurring inside the devolatilizing biomass particle are represented by conservation equations for mass, energy, and gas phase species. The resulting set of coupled non-linear governing equations is numerically solved to get the instantaneous state of the biomass particle. A short description of the governing equations and the boundary conditions is provided here.
%%
\subsubsection{Mass conservation}
%%
\underline{Solid phase}
%%
\begin{equation}
\dot{\rho}_{s,i}=\dot{\omega}_{s,i}
\end{equation}
 %%
where $\dot{\rho}_{s,i}$ is the rate of change of density of solid component $i = 1\ldots n_s$ and $\dot{\omega}_{s,i}$ is the reaction source term for solid component $i$ obtained from the chemical kinetic scheme.

\underline{Gas phase}

\begin{itemize}
\item Continuity Equation:
\begin{equation}
\frac{\partial \left(\varepsilon\rho \right)}{\partial t}  + \frac{1}{r^2} \frac{\partial \left(r^2\rho U \right)}{\partial r}  = \Sigma R_j\,,
\end{equation}
 %%
where $\varepsilon$ is the particle porosity, $\rho$ is the gas density, $U$ is superficial velocity of the gas, and $R_j$ is the source term for gas phase species $j=1\ldots n_g$. Porosity, $\varepsilon$ is calculated as $\varepsilon=1-\frac{\rho_s}{\rho_0}\left(1-\varepsilon_0 \right)$, here $\rho_s$ is the instantaneous particle density, $\rho_0$ is the initial particle density, and $\varepsilon_0$ is the initial porosity.

A low Mach number assumption is made to decouple the effect of pressure on density. Gas phase is assumed to be ideal. Therefore, $\rho$ can be calculated from the equation of state, as $\rho=\frac{PM}{RT}$ and $U$ is calculated through the continuity equation. Here, $P$ is the thermodynamic pressure assumed to be constant, $M$ is the molecular weight of the gas phase, $R$ is the ideal gas constant.

\item Species Conservation Equation:
%%
\begin{equation}
\frac{\partial (\varepsilon \rho y_{_j})}{\partial t} + \frac{1}{r^2} \frac{\partial (r^2U\rho y_{j})}{\partial r} = \frac{1}{r^2} \frac{\partial}{\partial r}\left( r^2 D\frac{\partial  \rho y_{j}}{\partial r}\right)+R_j\,,
\end{equation}
%%
where $y_{j}$ is the mass fraction of gas species $j$ and $D$ is effective diffusivity taken as a constant.
\end{itemize}
%%
\subsubsection{Energy Conservation}
For the energy equation, a local thermal equilibrium between gas and solid phase is assumed.
\begin{equation}
(\rho C_p)_{p} \frac{\partial T}{\partial t} + (\rho C_p)_{g}U\frac{\partial T}{\partial r} =
\frac{1}{r^2} \frac{\partial}{\partial r} \left(r^2\lambda \frac{\partial T}{\partial r} \right) + Q\,,
\end{equation}
%%
where $T$ is the particle temperature, $(\rho C_p)_{p}$ is the total heat capacity of the particle, $(\rho C_p)_{g}$ is the heat capacity of the gas phase, $Q$ is the heat source term, and $\lambda$ is the effective thermal conductivity, calculated as the weighted sum of solid components and gas phase conductivity, including the effect of radiative heat transfer through pores~\cite{di1996heat}. The resulting expression is written as:
\begin{equation}
 \lambda=(1-\eta)\lambda_w+\eta \lambda_c+\varepsilon\lambda_g+\frac{13.5\sigma T^3d}{e}
\end{equation}
where $\lambda_w$, $\lambda_c$, and $\lambda_g$ are the thermal conductivities of wood, char, and gas phase respectively, $\sigma$ is the Stefan-Boltzmann constant, $e$ is the pore emissivity, and $d$ is the effective pore diameter. $\eta$ is the degree of devolatilization, defined as $\eta=1-\frac{\rho_s-\rho_c}{\rho_w}$, here $\rho_s$, $\rho_c$, and $\rho_w$ are the densities of instantaneous solid and char, and initial wood. All the parameters are used from Corbetta et al.~\cite{corbetta2014pyrolysis}.   
%%
\subsubsection{Boundary Conditions}
%%
At the particle surface, species flux is imposed by convective mass transfer and thermal energy flux is imposed by convective and radiative heat transfer.

At the particle surface:
\begin{align}
D\left.\frac{\partial \rho y_{j}}{\partial r}\right|_{s} &= \alpha(\rho y_{j,s} - \rho y_{j,\infty})\\
\lambda\left.\frac{\partial T}{\partial r}\right|_{s}&=h(T_s-T_{\infty}) + e\sigma(T_f^4-T_s^4)
\end{align}
%%
where $\alpha$ is the convective heat transfer coefficient, $h$ is the convective mass transfer coefficient, $e$ is surface emissivity, $y_{j,s}$ is the mass fraction of gas species $j$ at the particle surface, $y_{j,\infty}$ is the ambient mass fraction of gas species $j$, $T_{\infty}$ is the ambient temperature, $T_f$ is the furnace temperature, and $T_s$ is the particle surface temperature.

At the particle center, a zero gradient condition is imposed due to symmetry:
\begin{equation}
\left.\frac{\partial T}{\partial r}\right|_{r=0}=\left.\frac{\partial \rho y_{j}}{\partial r}\right|_{r=0}=0
\end{equation}
%%
It is assumed that there is no initial moisture present. Particle size is kept constant such that the loss of solid phase due to devolatilization is incorporated as an increase in porosity. Moreover, in a 1D model, anisotropy and inhomogeneity of the particle can not be incorporated, therefore, particle properties are averaged over all three directions (radial, tangential, and grain). These assumptions are commonly used in the literature~\cite{park2010experimental,ranzi2014kinetic}.
 %%
% ======================================== %
%% Homogeneous particle model
% ======================================== %
\subsection{Description of the homogeneous particle model}
%%
In the homogeneous particle model, all the intra-particle processes are neglected, implying that there is no gradient inside the particle. All the particle properties are function of time only, not space. Due to their simplicity, these models are most commonly used in CFD simulations of biomass thermochemical conversion~\cite{gera2001moisture,kaer1998numerical,koufopanos1991modelling}. The governing equations for the homogeneous particle model used in this work are described below.

\subsubsection{Mass Conservation}

\begin{itemize}
\item \underline{Solid phase}
\begin{equation}
\dot{\rho}_{s,i}=\dot{\omega}_{s,i}
\end{equation}
%
\item \underline{Gas phase}
\begin{equation}
\frac{\partial (\varepsilon \rho y_{j})}{\partial t}=R_j
\end{equation}
\end{itemize}
%
\vspace{-0.4in}
\subsubsection{Energy Conservation}
\begin{equation}
V_p(\rho C_p)_{p} \frac{\partial T}{\partial t} = hA_p(T_s-T_{\infty}) + e\sigma A_p(T_f^4-T_s^4) + QV_p
\end{equation}
%%
where $V_p$ and $A_p$ are volume and surface of the particle, respectively. All other symbols have the same meaning as described in the previous subsection.
%%

% ======================================== %
%% Results
% ======================================== %
\section{Results and Discussion}
\label{results}
%%
In the literature, most of the Euler-Lagrange modeling studies of biomass gasification use particle size in the range of 1.5 mm to 4 mm with a homogeneous particle model~\cite{overmann2008euler,oevermann2009euler,gerber2014two,ku2015cfd}. However, for particle size in this range, the homogeneous model assumptions might not be accurate enough for useful predictions. It is therefore essential to characterize the differences in predictions between homogeneous and resolved intra-particle models as a function of particle size and reactor conditions. For this purpose, an in-house intra-particle code is developed that can easily be interfaced with   CFD solvers able to simulate reactive fluidized bed reactors. The resulting intra-particle solver is first validated by comparing its predictions with existing experimental and numerical data~\cite{park2010experimental,corbetta2014pyrolysis}, as shown in Fig.~\ref{fig:valid}.
%%
\begin{figure}[h!]
      \centering
      \subfigure[Temperature]{\includegraphics[width=0.45\textwidth]{figures/validation/Tc}} 
      \subfigure[Solid mass fraction]{\includegraphics[width=0.45\textwidth]{figures/validation/ys}} \\                
       \caption{Comparison of particle center temperature and solid mass residue measured by Park et al.~\cite{park2010experimental} (symbols), predictions by Corbetta et al.~\cite{corbetta2014pyrolysis} using COMSOL (dashed lines), and the predictions of intra-particle model (solid lines). The simulations use a particle diameter of $d = $2.54 cm and a heat transfer coefficient $h = 20$ W/m$^2$K.}
        \label{fig:valid}
\end{figure}
%%

To compare the homogeneous and intra-particle models, an ambient gas temperature of $T_g$ = 1073~K and a convective heat transfer coefficient of h = 1000 W/m$^2$K are used for all test cases. These parameters are within the range of values found in biomass gasification in fluidized bed reactors~\cite{ku2015cfd,di2000modelling}. Average particle temperature, solid mass fraction, and levoglucosan (LVG) production rate and cumulative yield normalized by the initial particle mass are compared between the two models for particle diameters $d = 1$ mm and $d = 4$ mm. Results are shown in Figs.~\ref{fig:d1e-3} and~\ref{fig:d4e-3}. 
The average properties for the intra-particle model are calculated by volume averaging over the entire particle. Levoglucosan is chosen for comparison purposes as it is the main decomposition product of cellulose, and therefore a very important contributor to biomass devolatilization products.
%%
\begin{figure}[h!]
      \centering
      \subfigure[Temperature.\label{fig:d1e-3a}]{\includegraphics[width=0.45\textwidth]{figures/d1e-3/plots/T}} 
      \subfigure[Solid mass fractions.\label{fig:d1e-3b}]{\includegraphics[width=0.45\textwidth]{figures/d1e-3/plots/ys}} \\
      \subfigure[Production rate of levoglucosan.\label{fig:d1e-3c}]{\includegraphics[width=0.45\textwidth]{figures/d1e-3/plots/prod}} 
      \subfigure[Levoglucosan yield.\label{fig:d1e-3d}]{\includegraphics[width=0.45\textwidth]{figures/d1e-3/plots/yield}}                   
      \caption{Comparison of particle temperature, solid mass fraction, and production rate and yield of levoglucosan obtained from intra-particle (red solid lines) and homogeneous model (blue dashed lines) for $d = 1$ mm}
       \label{fig:d1e-3}
\end{figure}
%%
%%
\begin{figure}[h!]
      \centering
      \subfigure[Temperature.\label{fig:d4e-3a}]{\includegraphics[width=0.45\textwidth]{figures/d4e-3/plots/T}} 
      \subfigure[Solid mass fractions.\label{fig:d4e-3b}]{\includegraphics[width=0.45\textwidth]{figures/d4e-3/plots/ys}} \\
      \subfigure[Production rate of levoglucosan.\label{fig:d4e-3c}]{\includegraphics[width=0.45\textwidth]{figures/d4e-3/plots/prod}} 
      \subfigure[Levoglucosan yield.\label{fig:d4e-3d}]{\includegraphics[width=0.45\textwidth]{figures/d4e-3/plots/yield}}                   
       \caption{Comparison of particle temperature, solid mass fraction, and production rate and yield of levoglucosan obtained from intra-particle (red solid lines) and homogeneous model (blue dashed lines) for $d = 4$ mm}
        \label{fig:d4e-3}
\end{figure}
%%

By comparing Figs.~\ref{fig:d1e-3} and~\ref{fig:d4e-3}, it is clear that the deviation between the predictions of the homogeneous model and the intra-particle model increases with the particle size. Figs.~\ref{fig:d1e-3a} and~\ref{fig:d4e-3a} compare the evolution of the average particle temperature, showing that the homogenous particle model predicts faster heating of the particle compared to the intra-particle model. This can be explained by observing that when intra-particle transport is resolved, the surface temperature ($T_s$) of the particle rises much faster than the inner locations. In contrast in the homogenous particle model, absorbed heat is distributed uniformly throughout the particle, thereby decreasing the particle temperature below $T_s$. This results in an higher temperature difference between the particle and its surrounding gas in the homogenous model, hence a higher rate of heat transfer as convective heat transfer rate is directly proportional to the temperature difference. 

Faster rate of heat transfer in the homogeneous model causes faster conversion of biomass and higher production rates of the gas species. This can be seen in Figs.~\ref{fig:d1e-3b} and~\ref{fig:d4e-3b} for the evolution of the solid mass fraction, and in Figs.~\ref{fig:d1e-3c} and~\ref{fig:d4e-3c} for the levoglucosan production rate. For $d = 4$ mm, differences between homogeneous and intra-particle model are too large to be ignored, especially in terms of the biomass conversion time, which for the intra-particle model is more than 3 times that observed in with the homogeneous particle assumption. This has the potential to significantly impact the biomass dynamics and predictions in FBR simulations. It is interesting to note though that the final yield of the products is almost the same for both models. 

\begin{wrapfigure}[38]{r}{0.55\textwidth}
\centering
\includegraphics[width=0.4\textwidth]{figures/FB}
\includegraphics[width=0.4\textwidth]{figures/droptube}
\vspace{-0.1in}
\caption{\small Detailed simulations of reactive particle-laden flows in NGA. A. Cluster-induced heterogeneities in catalytic bio-vapor cracking in a turbulent riser flow~\cite{CapecelatroCEJ2014}. B. Individual tar species production during biomass gasification in a pseudo-2D fluidized bed reactor. C.  Average species mass fractions along the reactor axis during gasification in a drop tube - Comparison between simulation (line) and experimental data (symbols,~\cite{DupontF2008}.)}
\label{NGA}
\end{wrapfigure}In summary, the present study shows that in the context of biomass gasification, predictions obtained using an homogenous particle assumption deviate significantly from the case where the intra-particle transport processes are resolved. As expected, the discrepancies are more marked for larger particle diameters, but are already non-negligible at $d = $1 mm.   

\section{Conclusions and Perspectives}

In this work, 1D spherical intra-particle model and a homogeneous model for biomass particle devolatilization are developed and implemented in a stand-alone solver. Predictions from both models are compared in various cases. It is found that for the range of particle sizes and reactor conditions typical of biomass gasification in fluidized bed reactors, homogeneous model predictions deviate significantly from the more comprehensive intra-particle treatment, especially in terms of   
chemical compound production rates and overall conversion time. To further characterize those differences in realistic flow configurations, especially regarding the impact on secondary gas phase reactions, the intra-particle 1D solver is being integrated within the Lagrange-Euler multiphase flow solver NGA~\cite{DesjardinsNGA2008}, used in previous work to investigate complex reactive particle-laden flows (Fig~\ref{NGA}). The fully coupled simulation of the conversion of a small amount of biomass in a lab scale fluidized bed reactor will then be performed, and compared to results obtained using an homogeneous particle assumption. 

\newpage
\bibliographystyle{wssci}
\bibliography{references}
\end{document}  

%
%\\
%\subsection{Description of fluidized bed solver}
%%%
%Reactive gas-solid fluidized bed reactor, used for the biomass gasification, is simulated using an in-house code NGA, which has been previously used in simulating reactive flows~\cite{capecelatro2015numerical}, biomass pyrolysis and gasification~\cite{pepiot2010chemical}, and detailed simulations of numerous configurations~\cite{desjardins2008accurate,desjardins2009spectrally,van2010ghost,desjardins2006modeling,knudsen2008dynamic,knudsen2009general,wang2007prediction,pepiot2010direct,pepiot2012numerical,capecelatro2013euler,capecelatro2014numerical} A brief description of NGA  is provided here.
%
%The flow of solid spherical particles suspended in a variable density, incompressible carrier fluid is solved in an Eulerian-Lagrangian framework, where the displacement of an individual particle $i$ is calculated using Newton's second law of motion:
%%%
%\begin{align}
%\frac{d \bold{u}_p}{d t} = \mathcal{A}^{(i)} + \mathcal{F}_c^{(i)}+g
%\end{align}
%%%
%where $\mathcal{A}$ is the interphase momentum exchange term, and $\mathcal{F}_c$ is the collision force modeled using a modified soft-sphere approach originally proposed by~\cite{cundall1979discrete}. Additional ODEs are defined to calculate the time evolution of the reactive particles' temperature and compositions, along with gas phase product formation:
%%%
%\begin{align}
%\rho_pC_p\frac{d T_p}{d t} &= \mathcal{B}^{(i)} + \dot{\omega}_T,\\
%\frac{d m^{(i)}_{S,k}}{d t} &= \dot{\omega}_{S,k},\\
%\frac{d m^{(i)}_{G,k}}{d t} &= \dot{\omega}_{G,k} = \mathcal{C}^{(i)}_k 
%\end{align}
%%%
%In these equations, $T_p$, $\rho_p$ and $C_p$ are the particle temperature, density, and specific heat capacity, respectively, $m_{S,k}$ is the total mass of solid component $k$, $m_{G,k}$ is the mass of gas phase product $k$ released by the reacting particle, $\dot{\omega}_T$, $ \dot{\omega}_{S,k}$, and $\dot{\omega}_{G,k}$ are mass production rates obtained from the prescribed solid phase chemical kinetic scheme, and $\mathcal{B}$ and $\mathcal{C}^{(i)}_k$  are interphase heat and mass exchange terms. 
%
%To account for the presence of the particle phase in the fluid without requiring to resolve the boundary layers around individual particles, a volume filter is applied to the low Mach number Navier-Stokes equations following~\cite{anderson1967fluid}, thereby replacing the point variables (fluid velocity, pressure, etc.) by smoother, locally filtered fields, as described in~\cite{capecelatro2013euler,capecelatro2014numerical,capecelatro2015numerical}. The relationship between the interphase exchange term seen by the fluid $\tilde{\mathcal{A}}$ , and that seen by an individual particle i, $\mathcal{A}^{(i)}$, is given by
%%%
%\begin{align}
%\epsilon_f \tilde{\mathcal{A}}(\bold{x},t) = \Sigma_{i=1}^k \mathcal{A}^{(i)}(t)G(|\bold{x}-\bold{x}_p^{(i)}|)V_p,
%\end{align}
%%%
%where $\epsilon_f $ is the filtered gas volume fraction, $N_p$ is the total number of particles, $V_p = \pi d_p^3/6$ is the particle volume, and $G$ is a filtering kernel with a characteristic length $\delta_f\gg d_p$, such that $G(r) > 0$ decreases monotonically with increasing $r$, and is normalized such that it integrates to unity. Details on the choice of $G$ and $\delta_f$, and the actual implementation of Eq. 5 is provided in [50]. The fluid interphase heat and mass transfer terms are obtained from $\mathcal{B}$ and $\mathcal{C}_k$ in a similar manner, and appear in the transport equations solved for gas phase temperature and species mass fractions. Mixture-averaged diffusion coefficients are used to accurately describe species diffusion in the gas phase, while the chemical source term is readily available from the prescribed gas phase kinetic mechanism.
%
%This simulation strategy has proven to be very successful for simulating a wide range of dense fluid-solid particulate flows~\cite{capecelatro2013euler}, including catalytic conversion of bio-vapor in turbulent riser reactors, and biomass gasification with the compact chemistry model. For illustration purpose, figure~\ref{fig:example} shows few product species of biomass pyrolysis in FBR using NGA~\cite{ConfAICHE2013a}.
%%%
%\begin{figure}[htbp]
%   \centering
%   \includegraphics[width=0.3\textwidth]{figures/FBR_example} 
%   \caption{Product species scaled by their maximum value for biomass pyrolysis in FBR after 0.5$s$ of the biomass injection}
%   \label{fig:example}
%\end{figure}
